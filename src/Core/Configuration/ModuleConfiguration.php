<?php namespace Plumbus\Core\Configuration;

use Plumbus\Core\Controller\Exception\IllegalModuleConfiguration;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Plumbus\Traits\Core\RequestTrait;

class ModuleConfiguration
{
    use ProjectConfigurationTrait;
    use RequestTrait;

    const SSI_MODULE_TEMPLATE = 'internal/ssi_module.twig';
    /**
     * @var array
     */
    private $moduleConfiguration = [];

    /**
     * @var string
     */
    private $moduleKey;

    /**
     * @var bool
     */
    private $ssi = false;
    /**
     * @var int
     */
    private $ssiCacheTime = 0;

    public function __construct($moduleKey, array $moduleConfiguration = [])
    {
        if (!isset($moduleConfiguration['class'])) {
            throw new IllegalModuleConfiguration('"class" param missed for module');
        }
        if (!isset($moduleConfiguration['action'])) {
            throw new IllegalModuleConfiguration('"action" param missed for module');
        }
        $this->moduleConfiguration = $moduleConfiguration;
        $this->moduleKey = $moduleKey;

        if (!empty($moduleConfiguration['cache']) || !empty($moduleConfiguration['ssi'])) {
            /**
             * этот модуль будет отрисовываться посредством ssi с включенным nginx кешем
             */
            $maxSsiCacheTime = $this->configuration()->getMaxSsiCacheTime();
            $this->ssi = true;
            if (isset($moduleConfiguration['cache'])) {
                $this->ssiCacheTime = min($maxSsiCacheTime, max(0, intval($moduleConfiguration['cache'])));
            }
        }
    }

    /**
     * @return null|string
     */
    public function getTemplate()
    {
        return $this->ssi ? self::SSI_MODULE_TEMPLATE : 'module' . DIRECTORY_SEPARATOR . $this->moduleConfiguration['template'];
    }

    /**
     * @return bool
     */
    public function getIsSsi()
    {
        return $this->ssi;
    }

    public function setIsSsi(bool $isSsi)
    {
        $this->ssi = $isSsi;
    }

    /**
     * @return string
     */
    public function getModuleKey()
    {
        return $this->moduleKey;
    }

    /**
     * @return array
     */
    public function getCss():array
    {
        return $this->moduleConfiguration['css'] ?? [];
    }

    public function getJs():array
    {
        return $this->moduleConfiguration['js'] ?? [];
    }

    public function getSsiUrl(string $blockKey, array $writeResponse = null)
    {
        $variables = '';
        if ($writeResponse !== null) {
            $variables = urlencode(json_encode($writeResponse));
        }
        return '/ssi/?path=' . urlencode($this->getRequest()->getRequestUrl()) .
        '&block=' . urlencode($blockKey) .
        '&module=' . urlencode($this->moduleKey) .
        '&writeResponse=' . $variables;
    }

    public function getClass()
    {
        return $this->moduleConfiguration['class'] ?? null;
    }

    public function getAction()
    {
        return $this->moduleConfiguration['action'] ? 'action' . $this->moduleConfiguration['action'] : null;
    }

    /**
     * @return array
     */
    public function getPageViewSettings()
    {
        return [
            'css' => $this->moduleConfiguration['css'] ?? []
        ];
    }

    public function getCacheTime()
    {
        return $this->ssiCacheTime;
    }
}
