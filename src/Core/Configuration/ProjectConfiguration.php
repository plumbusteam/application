<?php namespace Plumbus\Core\Configuration;

use Plumbus\Core\Controller\Page\WebPage;
use Plumbus\Router;
use Plumbus\Injectable\InjectableComponentTrait;

class ProjectConfiguration
{
    use InjectableComponentTrait;
    /**
     * @var array
     */
    private $configurationArray;

    /**
     * @var string
     */
    private $coreRootPath;

    /**
     * @var string
     */
    private $rootPath;

    /**
     * @param string $settingPath
     * @param null $defaultValue
     * @return mixed|null
     */
    public function getSetting(string $settingPath, $defaultValue = null)
    {
        $settingPath = explode('.', $settingPath);
        $configuration = $this->configurationArray;
        while ($pathPart = array_shift($settingPath)) {
            if (isset($configuration[$pathPart])) {
                $configuration = $configuration[$pathPart];
            } else {
                return $defaultValue;
            }
        }
        return $configuration;
    }

    public static function instance(array $configurationArray = null)
    {
        static $instance;
        return (null !== $configurationArray) ? $instance = new self($configurationArray) : $instance;
    }

    /**
     * @param array $configurationArray
     * @throws \Exception
     */
    public function __construct(array $configurationArray)
    {
        $this->coreRootPath = __DIR__ . '/../../';
        $this->rootPath = $configurationArray['rootPath'] ?? null;
        if (!$this->rootPath) {
            throw new \Exception('rootPath missed in configuration');
        }
        $this->configurationArray = $configurationArray;
    }

    /**
     * @return string
     */
    public function getRoothPath()
    {
        return $this->rootPath;
    }

    public function getCoreRootPath()
    {
        return $this->coreRootPath;
    }

    /**
     * @return string
     */
    public function getProjectNamespace():string
    {
        return $this->configurationArray['project_namespace'] ?? '';
    }

    public function getRoutingMaps()
    {
        return $this->configurationArray['routing'] ?? [];
    }

    public function getRoutingTrailingSlashBehavior()
    {
        return $this->configurationArray['routing_trailing_slash_behavior'] ?? Router::TRAILING_SLASH_BEHAVIOR_IGNORE;
    }

    /**
     * @param string $pageIndex
     * @return WebPage
     */
    public function getPageConfiguration(string $pageIndex)
    {
        return $this->configurationArray['pages'][$pageIndex] ?? null;
    }

    public function getProjectTemplatesRoot()
    {
        return $this->configurationArray['templates_root'] ?? null;
    }

    public function getTemplatesCacheRoot()
    {
        return $this->configurationArray['templates_cache_root'] ?? '';
    }

    public function isDebugMode()
    {
        return $this->configurationArray['debug'] ?? false;
    }

    public function getMaxSsiCacheTime()
    {
        return $this->configurationArray['ssi_max_cache_time'] ?? 1;
    }

    public function getProjectDefaultCharset()
    {
        return $this->configurationArray['charset'] ?? 'utf-8';
    }

    public function getDatabaseConnection($connectionName)
    {
        return $this->configurationArray['db'][$connectionName] ?? null;
    }
}
