<?php namespace Plumbus\Core\User\BLL;

use Plumbus\Authorization\Social\User\SocialUser;
use Plumbus\Core\Module\BLL\DbTable;
use Plumbus\Injectable\InjectableComponentTrait;
use Project\Classes\Database\ConnectionsFactoryTrait;

class User extends DbTable
{
    use InjectableComponentTrait;
    use ConnectionsFactoryTrait;

    public function getById(int $userId)
    {
        return $this->getConnectionsFactory()->web->selectRow('SELECT * FROM `user` WHERE `id` = ?', [$userId]);
    }

    protected function getTableName():string
    {
        return 'user';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    public function updateSocialUser(SocialUser $user, int $networkId)
    {
        $profile = [
            'network_user_id' => $user->getId(),
            'network_id' => $networkId,
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'photo' => $user->getAvatarUrl(),
            'name' => $user->getFirstName() . ' ' . $user->getLastName()
        ];
        $this->getConnectionsFactory()->web->insert($this->getTableName(), $profile, array_keys($profile));
    }

    public function getAccess(int $userId, string $object, string $action)
    {
        return (bool) $this->getConnectionsFactory()->web->selectValue('
          SELECT
            `access`
          FROM
            `user_access`
          WHERE
            `user_id` = ? AND
            `object` = ? AND
            `action` = ? ',
            [
                $userId,
                $object,
                $action
            ]
        );
    }
}
