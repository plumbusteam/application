<?php namespace Plumbus\Core\User;

use Plumbus\Authorization\Social\User\SocialUser;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Traits\BLL\UserBLLTrait;
use Plumbus\Injectable\InjectableComponentTrait;
use Plumbus\Traits\Core\RequestTrait;

class CurrentUser
{
    const ROLE_ADMIN = 100;
    const ROLE_USER = 1;

    use InjectableComponentTrait;
    use RequestTrait;
    use UserBLLTrait;

    /**
     * @var int
     */
    private $userId;

    private $user;
    /**
     * @var bool
     */
    private $isGuest = true;


    private function initialize()
    {
        $currentUserId = $this->getRequest()->getSessionParam('uid', null);

        if (null === $currentUserId) {
            $this->getRequest()->setSessionParam('uid', 0);
        }
        if ($currentUserId) {
            $this->setIsGuest(false);
            $this->setUserId($currentUserId);
        }
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->userId) {
            return null;
        }
        return $this->user ?? $this->user = new User($this->userId);
    }

    public function __construct()
    {
        $this->initialize();
    }

    /**
     * @return int
     */
    public function getRole()
    {
        return $this->getIsGuest() ? 0 : self::ROLE_ADMIN;
    }

    public function authorizeBySocialProfile(SocialUser $user, int $networkId)
    {
        $this->getBllUser()->updateSocialUser($user, $networkId);
        $filter = new Filter();
        $filter->setCondition('network_id', $networkId);
        $filter->setCondition('network_user_id', $user->getId());
        $users = $this->getBllUser()->getByFilter($filter);

        if (!empty($users)) {
            $user = reset($users);
            $this->setUserId($user['id']);
            return $user['id'];
        }
        return false;
    }

    public function logout()
    {
        $this->userId = null;
        $this->setIsGuest(true);
        $this->getRequest()->setSessionParam('uid', null);
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
        $this->setIsGuest(false);
        $this->getRequest()->setSessionParam('uid', $userId);
        $this->getUser();
    }

    private function setIsGuest(bool $isGuest)
    {
        $this->isGuest = $isGuest;
    }

    /**
     * @return bool
     */
    public function getIsGuest()
    {
        return $this->isGuest;
    }

    /**
     * @param string $object
     * @param string $action
     * @return bool
     */
    public function access(string $object, string $action)
    {
        return $this->getIsGuest() ? false : $this->getUserAccess($object, $action);
    }

    /**
     * @param string $object
     * @param string $action
     * @return bool
     */
    private function getUserAccess(string $object, string $action)
    {
        return $this->getBllUser()->getAccess($this->userId, $object, $action);
    }
}
