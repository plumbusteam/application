<?php namespace Plumbus\Core\User;

use Plumbus\Traits\BLL\UserBLLTrait;

class User
{
    use UserBLLTrait;
    /**
     * @var int
     */
    private $userId;

    /**
     * @var array
     */
    private $userData;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * @param string $fieldName
     * @return null
     * @throws \Exception
     */
    public function getField(string $fieldName)
    {
        if (null === $this->userData) {
            $this->userData = $this->getBllUser()->getById($this->userId) ?? [];
        }
        return $this->userData[$fieldName] ?? null;
    }
}
