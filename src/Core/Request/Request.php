<?php
namespace Plumbus\Core\Request;

use Plumbus\Injectable\InjectableComponentTrait;


class Request
{
    use InjectableComponentTrait;

    /**
     * @var callable
     */
    private $shutDownHandler;
    /**
     * для пути /some/path?a=b будет содержать "/some/path?a=b"
     * @var string
     */
    private $relativeRequestUrl = '';

    /**
     * для пути /some/path?a=b будет содержать "/some/path"
     * @var string
     */
    private $relativeRequestFolder = '';

    /**
     * для пути /some/path?a=b будет содержать "a=b"
     * @var string
     */
    private $queryString = '';

    /**
     * @var array
     */
    private $get;

    /**
     * @var array
     */
    private $post;

    /**
     * @var array
     */
    private $cookie;

    /**
     * @var array
     */
    private $server;

    /**
     * @var array
     */
    private $files;

    /**
     * @var array
     */
    private $cookieToSend = [];

    /**
     * заголовки, которые мы отправи при завершении приложения клиенту
     * @var array
     */
    private $outHeaders = [];

    /**
     * @param callable $handler
     */
    public function setShutdownHandler(callable $handler)
    {
        $this->shutDownHandler = $handler;
    }

    public function populateGet()
    {
        $_GET = $this->get;
    }

    public function rePopulateGet()
    {
        unset($_GET);
    }

    /**
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->relativeRequestUrl;
    }

    /**
     * @param string $field
     * @param string|null $defaultValue
     * @return string
     */
    public function getQueryParam(string $field, string $defaultValue = null)
    {
        return array_key_exists($field, $this->get) ? $this->get[$field] : $defaultValue;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->get;
    }


    /**
     * @param string $field
     * @param string|null $defaultValue
     * @return string
     */
    public function getPostParam(string $field, string $defaultValue = null)
    {
        return array_key_exists($field, $this->post) ? $this->post[$field] : $defaultValue;
    }


    /**
     * @param string $name
     * @param string $header
     */
    public function addCustomHeader(string $name, string $header)
    {
        $this->outHeaders[$name] = ['message' => $header];
    }

    public function getHttpMethod()
    {
        return $this->server['REQUEST_METHOD'] ?? 'GET';
    }

    public function setCookie($cookieName, $cookieValue, $expire = 60)
    {
        $this->cookie[$cookieName] = $_COOKIE[$cookieName] = $cookieValue;
        $this->addCookie($cookieName, $cookieValue, $expire);
    }

    private function addCookie($cookieName, $cookieValue, $expire)
    {
        $this->cookieToSend[] = [$cookieName, $cookieValue, $expire];
    }

    public function setSessionParam($sesionValueKey, $sesionValueValue)
    {
        if (!isset($_SESSION) && !headers_sent()) {
            session_start();
        }
        $_SESSION[$sesionValueKey] = $sesionValueValue;
    }

    /**
     * @param $code
     * @param $message
     * @return $this
     */
    public function addHttpHeader(int $code, string $message)
    {
        $this->outHeaders['http'] = [
            'message' => $this->getServerProtocol() . ' ' . $code . ' ' . $message,
            'code' => $code
        ];
        return $this;
    }

    public function redirect($redirectUrl, $code = 302)
    {
        if ($redirectUrl && $redirectUrl[0] == '/') {
            $redirectUrl = (!empty($this->server['HTTPS']) ? 'https://' : 'http://') . $this->server['HTTP_HOST'] . $redirectUrl;
        }
        $this->outHeaders['http'] = [
            'message' => 'Location: ' . $redirectUrl,
            'code' => $code
        ];
        return $this;
    }

    private function getServerProtocol()
    {
        return $this->server['SERVER_PROTOCOL'] ?? 'HTTP/1.0';
    }

    /**
     * @param string $output
     */
    public function end(string $output = '')
    {
        $this->sendHeaders();
        echo $output;
        if ($this->shutDownHandler) {
            call_user_func($this->shutDownHandler);
        }
    }

    /**
     * @return $this
     */
    private function sendHeaders()
    {
        if (!headers_sent()) {
            foreach ($this->outHeaders as $header) {
                header($header['message'], true, $header['code'] ?? null);
            }
            foreach ($this->cookieToSend as $cookie) {
                setcookie($cookie[0], $cookie[1], time() + $cookie[2], '/');
            }
        }
        return $this;
    }

    public function initialize()
    {
        $this->relativeRequestUrl = $_SERVER['REQUEST_URI'] ?? $_SERVER['DOCUMENT_URI'] ?? '';
        $this->relativeRequestFolder = $_SERVER['DOCUMENT_URI'] ?? '';
        $this->queryString = $_SERVER['QUERY_STRING'] ?? '';
        $this->get = $_GET ?? [];
        $this->post = $_POST ?? [];
        $this->cookie = $_COOKIE ?? [];
        $this->server = $_SERVER ?? [];
        $this->files = $_FILES ?? [];
        $this->files = $_FILES ?? [];
        /**
         * Если реквест пришел по SSI, восстанавливаем исходный GET страницы
         */
        if ($this->getIsSSIRequest()) {
            $params = $this->setSsiModuleParams();
            $pathParts = explode('?', $params['path']);
            if (!empty($pathParts[1])) {
                parse_str($pathParts[1], $this->get);
            }
        }
        /**
         * использовать глобальные массивы в проекте больше нельзя
         */
        unset($_GET, $_POST, $_FILES);
    }

    /**
     * @param string $fileName
     * @return null
     */
    public function getFile(string $fileName)
    {
        return $this->files[$fileName] ?? null;
    }

    public function getSessionParam(string $field, string $defaultValue = null)
    {
        if (!isset($_SESSION) && !headers_sent()) {
            session_start();
        }
        $session = $_SESSION ?? [];
        return array_key_exists($field, $session) ? $session[$field] : $defaultValue;
    }

    public function getCookieParam(string $field, string $defaultValue = null)
    {
        return array_key_exists($field, $this->cookie) ? $this->cookie[$field] : $defaultValue;
    }

    public function getIsSSIRequest()
    {
        return !empty($this->server['SSI_MODULE']);
    }

    public function setSsiModuleParams()
    {
        parse_str($this->server['SSI_MODULE_PARAMS'], $params);
        return $params;
    }
}