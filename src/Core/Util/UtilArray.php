<?php namespace Plumbus\Core\Util;

class UtilArray
{
    /**
     * @param array $arrays
     * @return array|mixed
     */
    public static function mergeArrays(array $arrays)
    {
        $result = array_shift($arrays);
        while (!empty($arrays)) {
            $next = array_shift($arrays);
            if (is_array($next)) {
                foreach ($next as $key => $value) {
                    if (is_integer($key)) {
                        isset($result[$key]) ? $result[] = $value : $result[$key] = $value;
                    } elseif (is_array($value) && isset($result[$key]) && is_array($result[$key])) {
                        $result[$key] = self::mergeArrays(array($result[$key], $value));
                    } else {
                        $result[$key] = $value;
                    }
                }
            }
        }
        return $result;
    }
}

