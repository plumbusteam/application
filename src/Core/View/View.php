<?php namespace Plumbus\Core\View;

use Plumbus\Core\Controller\Page\WebPage;
use Plumbus\Core\View\Exception\IllegalPathException;
use Plumbus\Injectable\InjectableComponentTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;


class View
{
    use InjectableComponentTrait;
    use ProjectConfigurationTrait;

    /**
     * @param $layout
     * @param array $responseData
     * @param WebPage|null $pageSettings
     * @param bool|false $partial
     * @return string
     */
    public function render($layout, array $responseData, WebPage $pageSettings = null)
    {
        $projectTemplatesRoot = $this->configuration()->getProjectTemplatesRoot();
        if (null === $projectTemplatesRoot) {
            throw new IllegalPathException('No path specified for project templates');
        }

        $coreTemplatesRoot = $this->configuration()->getCoreRootPath() . DIRECTORY_SEPARATOR . 'templates';

        $loader = new \Twig_Loader_Filesystem(
            [
                $projectTemplatesRoot,
                $coreTemplatesRoot
            ]
        );
        $params = [];
        if (!$this->configuration()->isDebugMode()) {
            $params['cache'] = new \Twig_Cache_Filesystem($this->configuration()->getTemplatesCacheRoot());
        }
        $twig = new \Twig_Environment($loader, $params);

        $template = $twig->loadTemplate('internal/bootstrap.twig');

        ob_start();
        echo $template->render(
            [
                'layout' => 'layouts' . DIRECTORY_SEPARATOR . $layout . '.twig',
                'data' => $responseData,
                'debug' => $this->configuration()->isDebugMode(),
                'page' => $pageSettings,
            ]
        );
        return ob_get_clean();
    }
}