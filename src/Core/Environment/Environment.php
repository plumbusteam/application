<?php namespace Plumbus\Core\Environment;

use Plumbus\Injectable\InjectableComponentTrait;
use Dotenv\Dotenv;

class Environment
{
    use InjectableComponentTrait;

    public function init(string $rootPath)
    {
        (new Dotenv($rootPath))->load();
    }
}
