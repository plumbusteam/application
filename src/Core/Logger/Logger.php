<?php namespace Plumbus\Core\Logger;

use Plumbus\Injectable\InjectableComponentTrait;

class Logger
{
    use InjectableComponentTrait;

    /**
     * @var array
     */
    private $metrics = [];

    /**
     * @var int
     */
    private $index = 0;

    /**
     * @var int
     */
    private $level = 0;

    /**
     * @return bool
     */
    private function isEnabled()
    {
        return true;
    }

    /**
     * @param string $metricName
     * @param string $message
     * @return bool
     */
    public function start(string $metricName, string $message = '') : string
    {
        $index = $this->index++;
        if ($message) {
            $metricName .= ' [' . $message . ']';
        }
        if ($this->isEnabled()) {
            $this->metrics[$metricName][$index]['start'] = microtime(true);
            $this->metrics[$metricName][$index]['level'] = $this->level++;
        }
        return $metricName;
    }

    public function end(string $metricName) : bool
    {
        $metric = $this->metrics[$metricName];
        $keys = array_keys($metric);
        $index = end($keys);
        $metric = end($metric);
        $now = microtime(true);
        $this->level--;
        $this->metrics[$metricName][$index]['action'] = sprintf('%5f', $now - $metric['start']);
        $this->metrics[$metricName][$index]['time'] = $now;
        return true;
    }

    /**
     * @return array
     */
    public function getMetrics()
    {
        return $this->metrics;
    }

}
