<?php namespace Plumbus\Core\Module;

abstract class Base
{
    /**
     * @var string
     */
    protected $blockKey;
    /**
     * @var string
     */
    protected $moduleKey;

    /**
     * @param string $blockKey
     */
    public function setBlockKey(string $blockKey)
    {
        $this->blockKey = $blockKey;
    }

    /**
     * @param string $moduleKey
     */
    public function setModuleKey(string $moduleKey)
    {
        $this->moduleKey = $moduleKey;
    }

    /**
     * @return string
     */
    public function getBlockKey()
    {
        return $this->blockKey;
    }

    /**
     * @return string
     */
    public function getModuleKey()
    {
        return $this->moduleKey;
    }
}
