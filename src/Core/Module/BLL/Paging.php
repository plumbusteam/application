<?php namespace Plumbus\Core\Module\BLL;

class Paging
{
    /**
     * @var int
     */
    private $perPage = 1;

    /**
     * @var int
     */
    private $page = 1;

    /**
     * @var int
     */
    private $totalItemsCount;

    /**
     * @return string
     */
    public function getLimit():string
    {
        return ' LIMIT ' . (($this->page - 1) * $this->perPage) . ', ' . $this->perPage;
    }

    /**
     * @param int $itemsCount
     */
    public function setTotalItemsCount(int $itemsCount)
    {
        $this->totalItemsCount = $itemsCount;
    }

    public function getPagesCount()
    {
        if (null === $this->totalItemsCount) {
            throw new \Exception('totalItemsCount is empty');
        }
        return ceil($this->totalItemsCount / $this->perPage);
    }

    /**
     * @param int $firstGroupLength - количество ссылок на начальные страницы (3 страницы - 1, 2, 3..)
     * @param int $groupLength - количество отображаемых ссылок страниц вокруг ссылки на текущую страницу
     * @param int $lastGroupLength - количество ссылок на конечные страницы (2 страницы - ..19, 20)
     * @return array - массив страниц для отображения, например [1, 2, 5, 6, 7, 99, 100]
     * @throws \Exception
     */
    public function getPrettyPages(int $firstGroupLength = 3, int $groupLength = 3, int $lastGroupLength = 3)
    {
        $pages = [];
        if (null === $this->totalItemsCount) {
            throw new \Exception('totalItemsCount is empty');
        }

        $pagesCount = $this->getPagesCount();
        for ($page = 1; $page <= $firstGroupLength; $page++) {
            if ($page <= $pagesCount) {
                $pages[$page] = $page;
            }
        }

        $sizeLeft = max(1, floor($groupLength / 2));
        $sizeRight = max(2, ceil($groupLength / 2));

        for ($page = max(0, $this->page - $sizeLeft); $page < ($this->page + $sizeRight); $page++) {
            if ($page && ($page <= $pagesCount)) {
                $pages[$page] = $page;
            }
        }

        for ($page = max(0, $pagesCount - $lastGroupLength + 1); $page <= $pagesCount; $page++) {
            if ($page && ($page <= $pagesCount)) {
                $pages[$page] = $page;
            }
        }

        return $pages;
    }

    /**
     * @param int $perPage
     * @return $this
     */
    public function setPerPage(int $perPage)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page)
    {
        $this->page = $page;
        return $this;
    }
}
