<?php namespace Plumbus\Core\Module\BLL;

class Filter
{
    /**
     * @var Paging
     */
    private $paging;

    /**
     * @var array
     */
    private $conditions = [];

    public function setPaging(Paging $paging)
    {
        $this->paging = $paging;
    }

    public function setCondition(string $field, $value)
    {
        $this->conditions[$field] = $value;
    }

    public function getSqlWhere(array &$parameters)
    {
        $where = [];
        foreach ($this->conditions as $field => $value) {
            $where[$field] = $field . '=?';
            $parameters[$field] = $value;
        }
        return $where ? (' WHERE (' . implode(' AND ', $where) . ') ') : '';
    }

    public function getLimit():string
    {
        $limit = '';
        if ($this->paging) {
            return $this->paging->getLimit();
        }
        return $limit;
    }
}
