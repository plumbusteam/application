<?php namespace Plumbus\Core\Module\BLL;

use Project\Classes\Database\ConnectionsFactoryTrait;

abstract class DbTable extends Base
{
    use ConnectionsFactoryTrait;

    abstract protected function getTableName():string;

    abstract protected function getIndexFieldName():string;

    public function getByFilter(Filter $filter, int &$totalCount = null)
    {
        $connection = $this->getConnectionsFactory()->web;
        $queryParams = [];
        $limit = $filter->getLimit();
        $fields = '*';
        $where = $filter->getSqlWhere($queryParams);

        $order = '';
        $query = 'SELECT SQL_CALC_FOUND_ROWS ' . $fields . ' FROM ' . $this->getTableName() . $where . $order . $limit;
        $result = $connection->selectKeyRow($query, array_values($queryParams));
        if (null !== $totalCount) {
            $totalCount = $connection->selectValue('SELECT FOUND_ROWS()');
        }
        return $result;
    }

    public function getById(int $id)
    {
        $query = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $this->getIndexFieldName() . ' = ?';
        return $this->getConnectionsFactory()->web->selectRow($query, [$id]);
    }

    public function update(int $id, array $data)
    {
        $data[$this->getIndexFieldName()] = $id;
        return $this->getConnection()->web->insert($this->getTableName(), $data, array_keys($data));
    }
}