<?php namespace Plumbus\Core\Router;

use Plumbus\Injectable\InjectableComponentTrait;

class Router extends \Plumbus\Router
{
    use InjectableComponentTrait;
}
