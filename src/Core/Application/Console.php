<?php namespace Plumbus\Core\Application;

use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Symfony\Component\Console\Application as SymfonyConsole;
use Symfony\Component\Console\Command\Command;

class Console extends Base
{
    use ProjectConfigurationTrait;

    /**
     * @var SymfonyConsole
     */
    private $application;

    /**
     * @param string $rootPath
     */
    public function __construct(string $rootPath)
    {
        parent::__construct($rootPath);

        $this->initApplication();
    }

    /**
     * @param SymfonyConsole[] $commands
     */
    public function addCommands(array $commands)
    {
        foreach ($commands as $command) {
            $this->application->add($command);
        }
    }

    public function run()
    {
        $this->application->run();
    }

    private function initApplication()
    {
        $this->application = new SymfonyConsole();
        foreach ($this->configuration()->getSetting('commands') as $commandClass) {
            if ($commandClass instanceof Command) {
                continue;
            }

            $this->application->add(new $commandClass());
        }
    }
}
