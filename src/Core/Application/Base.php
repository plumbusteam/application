<?php namespace Plumbus\Core\Application;

use Plumbus\Traits\Core\EnvironmentTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;

class Base
{
    /**
     * @var int
     */
    protected $oldUmask;

    use ProjectConfigurationTrait;
    use EnvironmentTrait;

    /**
     * @param string $rootPath
     */
    public function __construct(string $rootPath)
    {
        $this->oldUmask = umask(0000);
        $this->configuration(
            $this->initConfig($rootPath)
        );
    }

    /**
     * @param string $rootPath
     * 
     * @return array
     */
    protected function initConfig(string $rootPath)
    {
        $rootPath = rtrim($rootPath, '\/');
        $this->getEnvironment()->init($rootPath);
        $projectConfiguration = require_once $rootPath . '/config/project.php';
        $projectConfiguration['rootPath'] = $rootPath;

        return $projectConfiguration;
    }
}