<?php
namespace Plumbus\tests;

use Plumbus\Core\Application\Web;

class Application extends \PHPUnit_Framework_TestCase
{
    /**
     * Тестируем отрисовку индексной страницы и наличие в ответе строки '<!DOCTYPE html>'
     */
    public function testApplicationRunWithoutAnyRouting()
    {
        ob_start();
        $application = new Web(__DIR__ . '/../../../../project/');
        $application->run();
        $response = ob_get_clean();
        self::assertContains('<!DOCTYPE html>', $response);
    }
}
