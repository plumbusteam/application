<?php namespace Plumbus\Core\Application;

use Plumbus\Core\User\Exception\Access;
use Plumbus\Exception\NotFound;
use Plumbus\Exception\Redirect;
use Plumbus\Traits\Core\ControllerTrait;
use Plumbus\Traits\Core\LoggerTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Plumbus\Traits\Core\RequestTrait;
use Plumbus\Traits\Core\RouterTrait;

class Web extends Base
{
    use ProjectConfigurationTrait;
    use RequestTrait;
    use RouterTrait;
    use ControllerTrait;
    use LoggerTrait;

    /**
     * @param string $rootPath
     */
    public function __construct(string $rootPath)
    {
        parent::__construct($rootPath);

        session_set_cookie_params($this->configuration()->getSetting('session_lifetime', 0), "/");

        $this->getRequest()->setShutdownHandler(function () {
            $this->shutdownHandler();
        });
    }

    public function shutdownHandler()
    {
        $this->getLogger()->end('application');
        if ($this->configuration()->isDebugMode()) {
            echo "\n<!-- time metrics\n";
            foreach ($this->getLogger()->getMetrics() as $metric => $values) {
                foreach ($values as $value) {
                    if (!empty($value['action'])) {
                        echo str_repeat('--', $value['level'] + 1) . $value['action'] . ' ' . str_pad($metric,
                                30) . "\n";
                    }
                }
            }
            echo '-->' . "\n";
        }
        umask($this->oldUmask);
    }

    public function run()
    {
        $this->getLogger()->start('application');
        /**
         * инициализируем Request
         */
        $this->getRequest()->initialize();

        /**
         * инициализируем карту роутинга из конфига проекта
         */
        $this->setRoutingMap();

        if ($this->getRequest()->getIsSSIRequest()) {
            /**
             * Если это ssi запрос на модуль
             */
            $output = $this->getBeeControler()->runSsiModule();
        } else {
            /**
             * получаем конфигурацию контроллера из роутера
             */
            $routingResult = $this->resolveRouting();
            $output = '';
            if (false !== $routingResult) {
                list($pageKey, $variables) = $routingResult;
                /**
                 * Запускаем контроллер страницы
                 */
                $output = $this->runController($pageKey, $variables);
                if (false === $output) {
                    $output = '';
                }
            }
        }
        /**
         * Отдаем ответ в браузер
         */
        $this->getRequest()->end($output);
        return $pageKey??null;
    }

    private function setRoutingMap()
    {
        $behavior = $this->configuration()->getRoutingTrailingSlashBehavior();
        foreach ($this->configuration()->getRoutingMaps() as $requestTypeName => $routingMap) {
            $this->getRouter()->setRoutingMap($routingMap, $requestTypeName);
            $this->getRouter()->setTrailingSlashBehavior($behavior);
        }
    }

    /**
     * Находим запрашиваемыую страницу и заполняем переменные из URL. В случае ошибки (Страница не найдена или URL не соответствует
     * правилам конечного слеша) Производим редирект или выдаем 404 ошибку
     * @return array|bool
     */
    private function resolveRouting()
    {

        $this->getLogger()->start(__METHOD__);
        $variables = [];
        $pageKey = '';
        try {
            $pageKey = $this->getRouter()->route($this->getRequest()->getRequestUrl(), $variables);
        } catch (NotFound $exception) {
            $this->getRequest()->addHttpHeader(404, $exception->getMessage())->end();
            return false;
        } catch (Redirect $exception) {
            $this->getRequest()->redirect($exception->getRedirectUri(), 302)->end();
            return false;
        }

        $this->getLogger()->end(__METHOD__);
        return [$pageKey, $variables];
    }

    /**
     * @param string $pageKey
     * @param array $variables
     * @return bool|string
     * @throws \Exception
     */
    private function runController(string $pageKey, array $variables)
    {
        $interval = $this->getLogger()->start(__METHOD__, $pageKey);
        $result = '';
        try {
            $result = $this->getBeeControler()->runPage($pageKey, $variables);
        } catch (\Exception $exception) {
            if ($this->configuration()->isDebugMode()) {
                $this->getRequest()->addHttpHeader(502, 'Error')->end('');
                throw $exception;
            }
            $exception = $exception->getPrevious() ?? $exception;
            switch (get_class($exception)) {
                case NotFound::class:
                    $this->getRequest()->addHttpHeader(404, $exception->getMessage())->end();
                    break;
                case Redirect::class:
                    $this->getRequest()->redirect($exception->getRedirectUri(), 302)->end();
                    break;
                case Access::class:
                    $this->getRequest()->addHttpHeader(403, 'Forbidden')->end('');
                    break;
                default:
                    $this->getRequest()->addHttpHeader(502, 'Error')->end('');
                    break;
            }
            return false;
        }
        $this->getLogger()->end($interval);
        return $result;
    }
}