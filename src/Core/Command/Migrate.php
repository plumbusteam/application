<?php namespace Plumbus\Core\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Migrate extends Command
{
    protected function configure()
    {
        $this
            ->setName('migrate')
            ->setDescription('Run migrations.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Migrate.');
    }
}
