<?php
namespace Plumbus\Core\Controller\Page;

use Plumbus\Core\Configuration\ModuleConfiguration;
use Plumbus\Core\Util\UtilArray;
use Plumbus\Traits\Core\ProjectConfigurationTrait;

abstract class WebPage implements WebPageInterface
{
	use ProjectConfigurationTrait;

	const DEFAULT_ENCODING = 'utf-8';

	const DEFAULT_LAYOUT = 'index';
	/**
	 * @var array
	 */
	private $pageSettings;

	/**
	 * @param array $pageSettings
	 */
	public function __construct(array $pageSettings = [])
	{
		$this->pageSettings = $pageSettings;
	}

	public static function __set_state(array $pageSettings)
	{
		$class = get_called_class();
		return new $class($pageSettings['pageSettings']);
	}

	public function getRole()
	{
		return $this->pageSettings['role'] ?? 0;
	}

	/**
	 * @return ModuleConfiguration[][]
	 */
	public function getBlocks():array
	{
		$result = [];
		$blocks = $this->pageSettings['blocks'] ?? [];
		foreach ($blocks as $blockKey => $modules) {
			foreach ($modules as $moduleKey => $moduleConfigurationArray) {
				$result[$blockKey][$moduleKey] = new ModuleConfiguration($moduleKey, $moduleConfigurationArray);
			}
		}
		return $result;
	}

	/**
	 * @param string $blockKey
	 * @param string $moduleKey
	 * @return ModuleConfiguration
	 */
	public function getModuleConfiguration(string $blockKey, string $moduleKey) : ModuleConfiguration
	{
		$blocks = $this->getBlocks();
		return $blocks[$blockKey][$moduleKey];
	}

	public function getCacheTime():int
	{
		return isset($this->pageSettings['cache']) ? (int) $this->pageSettings['cache'] : 0;
	}

	public function getPageViewSettings():WebPage
	{
		return $this;
	}

	public function getLayout():string
	{
		return $this->pageSettings['layout'] ?? self::DEFAULT_LAYOUT;
	}

	public function getTitle():string
	{
		return $this->pageSettings['title'] ?? '';
	}

	public function getPageCharset()
	{
		return $this->configuration()->getProjectDefaultCharset() ?? self::DEFAULT_ENCODING;
	}

	public function getDescription():string
	{
		return $this->pageSettings['description'] ?? '';
	}

	public function getKeyWords():string
	{
		return $this->pageSettings['keywords'] ?? '';
	}

	/**
	 * @return mixed
	 */
	public function getCss():array
	{
		$css = $this->pageSettings['css'] ?? [];
		foreach ($this->getBlocks() as $blockKey => $modules) {
			foreach ($modules as $moduleKey => $module) {
				/**
				 * @var $module ModuleConfiguration
				 */
				$css = UtilArray::mergeArrays([$css, $module->getCss()]);
			}
		}

		return $css;
	}

	/**
	 * @return mixed
	 */
	public function getJs():array
	{
		$js = $this->pageSettings['js'] ?? [];
		foreach ($this->getBlocks() as $blockKey => $modules) {
			foreach ($modules as $moduleKey => $module) {
				/**
				 * @var $module ModuleConfiguration
				 */
				$js = UtilArray::mergeArrays([$js, $module->getJs()]);
			}
		}
		return $js;
	}
}
