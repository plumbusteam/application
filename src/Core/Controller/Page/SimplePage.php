<?php namespace Plumbus\Core\Controller\Page;

class SimplePage extends WebPage
{
    /**
     * @var array
     */
    private $variables;

    public function setVariables(array $variables = null)
    {
        $this->variables = $variables;
    }

    protected function getVariables()
    {
        return $this->variables;
    }

    protected function getVariable(string $variableName, $defaultValue = null)
    {
        return $this->variables[$variableName] ?? $defaultValue;
    }
}
