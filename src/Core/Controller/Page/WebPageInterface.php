<?php namespace Plumbus\Core\Controller\Page;

interface WebPageInterface
{
    public function getLayout():string;

    public function getTitle():string;

    public function getDescription():string;

    public function getKeyWords():string;

    public function getCss():array;

    public function getJs():array;

    public function setVariables(array $variables = null);

    public function getBlocks():array;

    public function getCacheTime():int;
}
