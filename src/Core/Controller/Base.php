<?php namespace Plumbus\Core\Controller;

use Plumbus\Injectable\InjectableComponentTrait;

abstract class Base
{
    use InjectableComponentTrait;

    abstract public function runPage(string $pageIndex, array $urlVariables = []);
}

