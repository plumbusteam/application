<?php
namespace Plumbus\Core\Controller;

use Plumbus\Core\Configuration\ModuleConfiguration;
use Plumbus\Core\Controller\Exception\IllegalPageConfiguration;
use Plumbus\Core\Controller\Exception\MissedPageConfiguration;
use Plumbus\Core\Controller\Page\WebPageInterface;
use Plumbus\Core\Module\Base;
use Plumbus\Core\User\Exception\Access;
use Plumbus\Core\Util\UtilArray;
use Plumbus\Exception\NotFound;
use Plumbus\Traits\Classes\User\CurrentUserTrait;
use Plumbus\Injectable\InjectableComponentTrait;
use Plumbus\Traits\Core\LoggerTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Plumbus\Traits\Core\RequestTrait;
use Plumbus\Traits\Core\RouterTrait;
use Plumbus\Traits\Core\ViewTrait;

/**
 * Контроллер по набору конфигураций страницы и модулей запускает модули
 * и отрисовывает результирующую страницу. Забивает лайаут данными как
 * пчёлка пчелиные соты
 *
 * Class Bee
 * @package Plumbus\Core\Controller
 */
class Controller extends \Plumbus\Core\Controller\Base
{
    use ProjectConfigurationTrait;
    use InjectableComponentTrait;
    use ViewTrait;
    use RequestTrait;
    use RouterTrait;
    use LoggerTrait;
    use CurrentUserTrait;
    /**
     * @var array
     */
    private $urlVariables = [];

    /**
     * @var string
     */
    private $pageIndex;

    /**
     * @var array
     */
    private $responseData = [];

    /**
     * @var array
     */
    private $writeResponse = [];

    /**
     * @var WebPageInterface
     */
    private $pageConfiguration;

    /**
     * @param string $pageIndex
     * @param array $urlVariables
     * @return string
     * @throws Access
     * @throws NotFound
     */
    public function runPage(string $pageIndex, array $urlVariables = [])
    {
        $this->urlVariables = $urlVariables;
        $this->pageIndex = $pageIndex;
        try {
            $this->pageConfiguration = $this->preparePageConfiguration($pageIndex, $urlVariables);
        } catch (MissedPageConfiguration $exception) {
            throw new NotFound('Missed page configuration for page ' . $pageIndex);
        } catch (IllegalPageConfiguration $exception) {
            throw new NotFound('Illegal page configuration for page ' . $pageIndex);
        }

        /**
         * Выполняем модули записи (сабмит форм)
         */
        $interval = $this->getLogger()->start(__METHOD__, 'execute write');
        $this->executeWriteModules();
        $this->getLogger()->end($interval);

        if ($this->getCurrentUser()->getRole() < $this->pageConfiguration->getRole()) {
            throw new Access('Cant access to page');
        }

        $this->responseData = [];
        foreach ($this->pageConfiguration->getBlocks() as $blockKey => $moduleConfigurations) {
            foreach ($moduleConfigurations as $moduleKey => $moduleConfiguration) {
                $template = '';
                $this->responseData[$blockKey][$moduleKey] = $this->runModule($blockKey, $moduleConfiguration,
                    $template);
                $this->responseData[$blockKey][$moduleKey]['template'] = $template;
            }
        }
        /**
         * выставляем время кеша на всю страницу
         */
        $nginxCacheTime = $this->pageConfiguration->getCacheTime();
        if ($nginxCacheTime) {
            $this->getRequest()->addCustomHeader('nginx-cache',
                'X-Accel-Expires: ' . $nginxCacheTime);
        }

        $pageViewSettings = $this->pageConfiguration->getPageViewSettings();
        return $this->render($this->pageConfiguration->getLayout(), $pageViewSettings);
    }

    private function executeWriteModules(string $currentModuleBlockKey = null, string $currentModuleModuleKey = null)
    {
        if ($this->getRequest()->getPostParam('writemodule')) {
            $blockKey = $this->getRequest()->getPostParam('block');
            $moduleKey = $this->getRequest()->getPostParam('module');
            if ($currentModuleBlockKey && (($currentModuleModuleKey != $moduleKey) || ($currentModuleBlockKey != $blockKey))) {
                return;
            }
            $blocks = $this->pageConfiguration->getBlocks();
            $action = 'action' . $this->getRequest()->getPostParam('action');
            /**
             * @var ModuleConfiguration $moduleConfiguration
             */
            if (empty($blocks[$blockKey][$moduleKey])) {
                return;
            }
            $moduleConfiguration = $blocks[$blockKey][$moduleKey];

            $class = $moduleConfiguration->getClass();
            $module = new $class;
            /**
             * @var Base $module
             */
            $module->setBlockKey($blockKey);
            $module->setModuleKey($moduleKey);

            $this->writeResponse[$blockKey][$moduleKey] = $module->$action($this->urlVariables);
        }
    }

    /**
     * @return string
     * @throws \Plumbus\Exception\NotFound
     * @throws \Plumbus\Exception\Redirect
     */
    public function runSsiModule()
    {
        $interval = $this->getLogger()->start(__METHOD__);
        $ssiModuleParams = $this->getRequest()->setSsiModuleParams();
        $variables = [];

        $blockKey = $ssiModuleParams['block'];
        $moduleKey = $ssiModuleParams['module'];
        $pageKey = $this->getRouter()->route($ssiModuleParams['path'], $variables);
        $this->urlVariables = $variables;
        $this->pageConfiguration = $this->configuration()->getPageConfiguration($pageKey);
        $moduleConfiguration = $this->pageConfiguration->getModuleConfiguration($blockKey, $moduleKey);

        $this->executeWriteModules($blockKey, $moduleKey);
        /**
         * Т.к. модуль отрисовывается, отрисовываем его а не шаблон для вставки ssi
         */
        $moduleConfiguration->setIsSsi(false);
        $cacheTime = $moduleConfiguration->getCacheTime();

        $this->getRequest()->addCustomHeader('nginx-cache', 'X-Accel-Expires: ' . $cacheTime);
        $template = '';
        $this->responseData['ssi'][$moduleKey] = $this->runModule($blockKey, $moduleConfiguration, $template);
        $this->responseData['ssi'][$moduleKey]['template'] = $template;
        $this->getLogger()->end($interval);
        return $this->render('ssi', null);
    }

    /**
     * @param ModuleConfiguration $moduleConfiguration
     * @param $template
     *
     * @return mixed
     */
    private function runModule(string $blockKey, ModuleConfiguration $moduleConfiguration, string &$template)
    {
        $moduleKey = $moduleConfiguration->getModuleKey();
        $interval = $this->getLogger()->start(__METHOD__,
            'block: ' . $blockKey . ', module: ' . $moduleKey);

        if ((null !== $this->getRequest()->getPostParam('writemodule'))) {
            $moduleConfiguration->setIsSsi(false);
        }
        /**
         * if POST param passwd - no ssi modules on page
         */
        if ($moduleConfiguration->getIsSsi()) {
            $moduleResponse = [
                'ssi_url' => $moduleConfiguration->getSsiUrl($blockKey,
                    $this->writeResponse[$blockKey][$moduleKey] ?? [])
            ];
        } else {
            $class = $moduleConfiguration->getClass();
            $module = new $class;
            /**
             * @var Base $module
             */
            $module->setBlockKey($blockKey);
            $module->setModuleKey($moduleKey);
            $action = $moduleConfiguration->getAction();
            $variables = UtilArray::mergeArrays([
                $this->urlVariables,
                $this->writeResponse[$blockKey][$moduleKey] ?? []
            ]);
            $moduleResponse = $module->$action($variables);
        }

        $template = $moduleConfiguration->getTemplate();
        $this->getLogger()->end($interval);
        return $moduleResponse;
    }

    /**
     * @param string $layout
     * @param array $pageSettings
     * @param bool|false $partial
     * @return string
     */
    private function render(string $layout, WebPageInterface $pageSettings = null)
    {
        $interval = $this->getLogger()->start(__METHOD__, 'layout: ' . $layout);
        $result = $this->getView()->render($layout, $this->responseData, $pageSettings);
        $this->getLogger()->end($interval);
        return $result;
    }

    /**
     * @param string $pageIndex
     * @return WebPageInterface
     * @throws IllegalPageConfiguration
     * @throws MissedPageConfiguration
     */
    private function preparePageConfiguration(string $pageIndex, array $urlVariables = null)
    {
        $interval = $this->getLogger()->start(__METHOD__, $pageIndex);
        $pageConfiguration = $this->configuration()->getPageConfiguration($pageIndex);
        if (null === $pageConfiguration) {
            throw new MissedPageConfiguration('missed configuration for page ' . $pageIndex);
        }
        if (!$pageConfiguration instanceof WebPageInterface) {
            throw new IllegalPageConfiguration('page config for page ' . $pageIndex . ' must be instance of WebPageInterface');
        }

        $pageConfiguration->setVariables($urlVariables);

        $this->getLogger()->end($interval);
        return $pageConfiguration;
    }
}