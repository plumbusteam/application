<?php namespace Plumbus\Traits\Classes\User;

use Plumbus\Core\User\CurrentUser;

trait CurrentUserTrait
{
    /**
     * @return CurrentUser
     */
    public function getCurrentUser()
    {
        return CurrentUser::instance();
    }
}
