<?php namespace Plumbus\Traits\Classes\User\Social\Network;

use Plumbus\Core\User\Social\Network\Vkontakte;

trait VkontakteTrait
{
    /**
     * @return Vkontakte
     */
    public function getNetworkVK()
    {
        return Vkontakte::instance();
    }
}
