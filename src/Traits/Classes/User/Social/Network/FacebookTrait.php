<?php namespace Plumbus\Traits\Classes\User\Social\Network;

use Plumbus\Core\User\Social\Network\Facebook;

trait FacebookTrait
{
    /**
     * @return Facebook
     */
    public function getNetworkFb()
    {
        return Facebook::instance();
    }
}
