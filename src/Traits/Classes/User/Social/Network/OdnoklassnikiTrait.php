<?php namespace Plumbus\Traits\Classes\User\Social\Network;

use Plumbus\Core\User\Social\Network\Odnoklassniki;

trait OdnoklassnikiTrait
{
    /**
     * @return Odnoklassniki
     */
    public function getNetworkOK()
    {
        return Odnoklassniki::instance();
    }
}
