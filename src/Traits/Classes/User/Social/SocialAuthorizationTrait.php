<?php namespace Plumbus\Traits\Classes\User\Social;

use Plumbus\Core\User\Social\Authorization;

trait SocialAuthorizationTrait
{
    /**
     * @return Authorization
     */
    public function getSocialAuthorization()
    {
        return Authorization::instance();
    }
}
