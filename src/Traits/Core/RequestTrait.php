<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\Request\Request;

trait RequestTrait
{
    /**
     * @return Request
     */
    public function getRequest()
    {
        return Request::instance();
    }
}
