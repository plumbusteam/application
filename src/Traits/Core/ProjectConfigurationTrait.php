<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\Configuration\ProjectConfiguration;

trait ProjectConfigurationTrait
{
    /**
     * @param array|null $configurationArray
     * @return ProjectConfiguration
     */
    public function configuration(array $configurationArray = null)
    {
       return ProjectConfiguration::instance($configurationArray);
    }
}