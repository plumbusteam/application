<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\Logger\Logger;

trait LoggerTrait
{
    /**
     * @return Logger
     */
    public function getLogger()
    {
        return Logger::instance();
    }
}
