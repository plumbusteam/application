<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\Router\Router;

trait RouterTrait
{
    /**
     * @return Router
     */
    public function getRouter()
    {
        return Router::instance();
    }
}
