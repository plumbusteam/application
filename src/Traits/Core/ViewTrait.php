<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\View\View;

trait ViewTrait
{
    /**
     * @return View
     */
    public function getView()
    {
        return View::instance();
    }
}
