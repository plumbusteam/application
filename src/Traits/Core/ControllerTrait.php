<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\Controller\Controller;

trait ControllerTrait
{
    /**
     * @return Controller
     */
    public function getBeeControler()
    {
        return Controller::instance();
    }
}