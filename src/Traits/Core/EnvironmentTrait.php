<?php namespace Plumbus\Traits\Core;

use Plumbus\Core\Environment\Environment;

trait EnvironmentTrait
{
    /**
     * @return Environment
     */
    public function getEnvironment()
    {
        return Environment::instance();
    }
}
