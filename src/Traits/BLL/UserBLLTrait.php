<?php namespace Plumbus\Traits\BLL;

use Plumbus\Core\User\BLL\User;

trait UserBLLTrait
{
    /**
     * @return User
     */
    public function getBllUser()
    {
        return User::instance();
    }
}
