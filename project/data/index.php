<?php
/**
 * Это важно! Заголовок должен отправляться сразу и именно в этом месте. Если заголовок отправлен не будет и произойдёт
 * ошибка, страница с ошибкой закешируется на время, прописанное в nginx по умолчанию.
 */
header('X-Accel-Expires: 0');
session_cache_limiter('public');
/**
 * При разработке должно быть включено - error_reporting(E_ALL), ini_set('display_errors', true)
 */
error_reporting(E_ALL);
ini_set('display_errors', true);

/**
 * Подключаем autoloader
 */
require_once __DIR__ . '/../../vendor/autoload.php';
/**
 * Создаем приложение с проектной конфигурацией
 */

$application = new \Plumbus\Core\Application\Web(
    __DIR__ . '/../'
);
/**
 * Запускаем приложение. Вывод будет выведен после обработки запроса.
 */
$application->run();