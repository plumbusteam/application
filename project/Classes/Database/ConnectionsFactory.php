<?php namespace Project\Classes\Database;

use Plumbus\Database\Connection;
use Plumbus\Injectable\InjectableComponentTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;

/**
 * @property Connection $web
 *
 * Class ConnectionsFactory
 * @package Project\Classes\Database
 */
class ConnectionsFactory extends \Plumbus\Database\ConnectionsFactory
{
    use ProjectConfigurationTrait;
    use InjectableComponentTrait;

    public function getConnectionSettings(string $connectionName):array
    {
        return $this->configuration()->getDatabaseConnection($connectionName);
    }
}
