<?php namespace Project\Classes\Database;


trait ConnectionsFactoryTrait
{
    /**
     * @return ConnectionsFactory
     */
    public function getConnectionsFactory()
    {
        return ConnectionsFactory::instance();
    }
}
