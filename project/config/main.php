<?php
return [
    /**
     * Namespace проекта
     */
    'project_namespace' => 'Project',
    /**
     * Путь до корня проекта
     */
    'rootPath' => __DIR__ . '/../',
    /**
     * настройка debug должна быть установлена в false на production серверах.
     * с включенным debug показывает дополнительную информацию о модулях в html коде вывода страниц,
     * не скрывает ошибки
     */
    'debug' => env('ENV', 'production') !== 'production',
    /**
     * Время действия сессии пользователя
     */
    'session_lifetime' => 604800,
    /**
     * Кодировка ответа
     */
    'charset' => 'utf-8',
    /**
     * routing - карта роутинга
     */
    'routing' => require_once 'routing.php',
    /**
     * настройки страниц - каждому пути в роутинге соответствует настройка для отрисовки ответа, запрошенного
     * по этому пути
     */
    'pages' => require_once 'pages.php',
    /**
     * поведение конечного слеша в URL адресе. Для предотвращения индексации адресов с и без "/" на конце
     * можно заставить приложение перенаправлять запросы на правильные URL
     */
    'routing_trailing_slash_behavior' => \Plumbus\Router::TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_EXISTS,
    /**
     * путь до папки с шаблонами проекта
     */
    'templates_root' => __DIR__ . '/../templates/',
    /**
     * путь до папки с кешем twig
     */
    'templates_cache_root' => __DIR__ . '/../templates/cache/',
    /**
     * максимальное время кеширования в ssi, в секундах. должно быть не более чем значение в
     * конфигурации nginx: fastcgi_cache_valid 200 301 302 304 60m;
     */
    'ssi_max_cache_time' => 3600,
    /**
     * Настройки соединений с базами данных
     */
    'db' => [
        'web' => [
            'dbname' => 'bee',
            'user' => 'bee',
            'password' => 'bee',
            'host' => 'localhost',
            'driver' => 'mysqli',
            'charset' => 'utf8'
        ]
    ],
];