<?php
return [
    \Plumbus\Router::DEFAULT_REQUEST_TYPE_NAME => [
        '' => 'index',
        'tests' => [
            'caching' => 'example_ssi_cached_page',
            'mailru_widget' => 'mailru_widget',
        ],
        'manual' => [
            'start' => 'manual/start',
        ],
        'news' => [
            '' => 'news_list',
            '%s' => 'news_item',
        ],
        'connect' => [
            '%s=network' => 'connect',
        ],
        'admin' => [
            'news' => [
                '' => 'admin_news_list',
                '%d=newsId' => [
                    '' => 'admin_news_item_edit'
                ]
            ]
        ]
    ],
];