<?php
$logger = \Plumbus\Core\Logger\Logger::instance();
/**
 * @var \Plumbus\Core\Logger\Logger $logger
 */
$interval = $logger->start('config:pages');
$modules['cache_example'] = [
    'class' => Project\Module\Cache\Example::class,
    'template' => 'example/cache.twig',
    'action' => 'showItem',
    'cache' => 5,
];

$modules['connect'] = [
    'class' => Project\Module\Connect\Connect::class,
    'template' => 'connect/connect.twig',
    'action' => 'showConnect',
];

$modules['test_table_of_contents'] = [
    'class' => \Project\Module\TOC\TOC::class,
    'template' => 'toc/toc.twig',
    'action' => 'showTableOfContents',
    'cache' => 20,
    'css' => [
        '/static/css/module/test_table_of_contents.css'
    ]
];

$modules['text_on_main'] = [
    'class' => \Project\Module\Html\Text::class,
    'template' => 'html/text/main.twig',
    'action' => 'showTextOnMain',
    'cache' => 10,
    'css' => [
        '/static/css/module/html/text/main.css'
    ]
];

$modules['mailru_widget'] = [
    'class' => \Project\Module\Html\Text::class,
    'template' => 'html/text/mailru/widget.twig',
    'action' => 'showMailruWidget',
];

$modules['manual_start'] = [
    'class' => \Project\Module\Html\Text::class,
    'template' => 'html/text/manual/start.twig',
    'action' => 'showManualStart',
    'ssi' => true,
    'css' => [
        '/static/css/module/html/text/manual.css'
    ]
];

$modules['news'] = [
    'class' => Project\Module\News\News::class,
    'template' => 'news/news_list.twig',
    'action' => 'showNewsList',
    'ssi' => true
];

$modules['auth_plank'] = [
    'class' => Project\Module\User\Authorization::class,
    'template' => 'user/authorization.twig',
    'action' => 'showAuthorization',
    'ssi' => true,
    'cache' => 0,
    'css' => [
        '/static/css/module/user/authorization.css'
    ],
    'js' => [
        'http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'
    ]
];

$pages = [
    'index' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'index',
        'cache' => 10,
        'css' => [
            '/static/css/project.css',
            '/static/css/index.css',
        ],
        'title' => 'Plumbus application engine',
        'description' => 'Plumbus application engine',
        'blocks' => [
            'head' => [
                'auth' => $modules['auth_plank'],
            ],
            'content' => [
                'text' => $modules['text_on_main'],
                'links' => $modules['test_table_of_contents']
            ]
        ],
    ]),
    'connect' => new \Plumbus\Core\Controller\Page\SimplePage(
        [
            'layout' => 'inner',
            'css' => [
                '/static/css/project.css',
                '/static/css/manual.css',
            ],
            'blocks' => [
                'head' => [
                    'auth' => $modules['auth_plank'],
                ],
                'content' => [
                    'connect' => $modules['connect']
                ]
            ],
        ]
    ),
    'manual/start' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'inner',
        'cache' => 10,
        'css' => [
            '/static/css/project.css',
            '/static/css/manual.css',
        ],
        'js' => [],
        'title' => 'Быстрый старт',
        'description' => 'Plumbus application engine',
        'blocks' => [
            'head' => [
                'auth' => $modules['auth_plank'],
            ],
            'content' => [
                'text' => $modules['manual_start']
            ]
        ],
    ]),
    'example_ssi_cached_page' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'tests/cache',
        'cache' => 20,
        'css' => [
            '/static/css/project.css',
            '/static/css/cached_page.css',
        ],
        'js' => [],
        'blocks' => [
            'head' => [
                'auth' => $modules['auth_plank'],
            ],
            'block1' => ['example_news_cached_module_1' => $modules['cache_example']],
            'block2' => [
                'example_news_cached_module_2' => \Plumbus\Core\Util\UtilArray::mergeArrays(
                    [
                        $modules['cache_example'],
                        [
                            'cache' => 10
                        ]
                    ])
            ],
            'block3' => [
                'example_news_cached_module_2' => \Plumbus\Core\Util\UtilArray::mergeArrays(
                    [
                        $modules['cache_example'],
                        [
                            'ssi' => true,
                            'cache' => 0
                        ]
                    ])
            ],
            'block4' => [
                'example_news_cached_module_2' => \Plumbus\Core\Util\UtilArray::mergeArrays(
                    [
                        $modules['cache_example'],
                        [
                            'cache' => 0,
                            'ssi' => false
                        ]
                    ])
            ]
        ]
    ]),
    'mailru_widget' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'inner',
        'css' => [
            '/static/css/project.css'
        ],
        'js' => [
            '//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js',
            '//api.torg.mail.ru/static/js/widget.min.js'
        ],
        'title' => 'Widget MailRu',
        'description' => 'Plumbus application engine',
        'blocks' => [
            'head' => [
                'auth' => $modules['auth_plank'],
            ],
            'content' => [
                'mailru_widget' => $modules['mailru_widget']
            ]
        ],
    ]),
    'admin_news_list' => new \Plumbus\Core\Controller\Page\SimplePage([
        'role' => \Plumbus\Core\User\CurrentUser::ROLE_ADMIN,
        'cache' => 0,
        'layout' => 'admin',
        'css' => [
            '/static/css/project.css',
            '/static/css/admin.css',
        ],
        'title' => 'Plumbus application engine',
        'description' => 'Plumbus application engine',
        'blocks' => [
            'head' => [
                'auth' => $modules['auth_plank'],
            ],
            'content' => [
                'news_list' => \Plumbus\Core\Util\UtilArray::mergeArrays([
                    $modules['news'],
                    [
                        'action' => 'adminShowNewsList',
                        'cache' => 0,
                        'ssi' => false,
                        'template' => 'admin/news_list.twig',
                    ]
                ])
            ]
        ],
    ]),
    'admin_news_item_edit' => new \Plumbus\Core\Controller\Page\SimplePage([
        'role' => \Plumbus\Core\User\CurrentUser::ROLE_ADMIN,
        'cache' => 0,
        'layout' => 'admin',
        'css' => [
            '/static/css/project.css',
            '/static/css/module/admin/news.css',
        ],
        'title' => 'Plumbus application engine',
        'description' => 'Plumbus application engine',
        'blocks' => [
            'head' => [
                'auth' => $modules['auth_plank'],
            ],
            'content' => [
                'news_list' => \Plumbus\Core\Util\UtilArray::mergeArrays([
                    $modules['news'],
                    [
                        'action' => 'adminShowNewsItemEdit',
                        'cache' => 0,
                        'ssi' => false,
                        'template' => 'admin/news_item.twig',
                    ]
                ])
            ]
        ],
    ]),
    'news_list' =>
        new \Plumbus\Core\Controller\Page\SimplePage([
            'layout' => 'inner',
            'cache' => 20,
            'css' => [
                '/static/css/project.css',
                '/static/css/manual.css',
            ],
            'js' => [],
            'title' => 'Новости',
            'description' => 'Plumbus application engine',
            'blocks' => [
                'head' => [
                    'auth' => $modules['auth_plank'],
                ],
                'content' => [
                    'news_list' => $modules['news']
                ]
            ],
        ])
];
$logger->end($interval);
return $pages;