-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: Plumbus
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'asdasd','rtuy'),(2,'<br>6asd','asdfsadf'),(3,'fedorkoasd',NULL),(4,'sashkasasd',NULL),(5,'ÐŸÐ Ð˜Ð’Ð•Ð•Ð•Ð•Ð•Ð•Ð•Ñ‚asd','Ð Ñ€Ð°Ð¾Ð»'),(6,'1456481558asd',NULL),(7,'1456481558asd',NULL),(8,'1456481558asd',NULL),(9,'1456481558asd',NULL),(10,'1456481559asd','457'),(11,'1456481559asd',NULL),(12,'1456481559asd',NULL),(13,'1456481560asd',NULL),(14,'1456481560asd',NULL),(15,'1456481560asd',NULL),(16,'1456481560asd',NULL),(17,'1456481561asd',NULL),(18,'1456481561asd',NULL),(19,'1456481561asd',NULL),(20,'1456481562asd',NULL),(21,'1456481562asd',NULL),(22,'1456481562asd',NULL),(23,'1456481563asd',NULL),(24,'1456481563asd',NULL),(25,'1456481563asd',NULL),(26,'1456481564asd',NULL),(27,'1456481564asd',NULL),(28,'1456481564asd',NULL),(29,'1456481565asd',NULL),(30,'1456481565asd',NULL),(31,'1456481565asd',NULL),(32,'1456481566asd',NULL),(33,'1456481566asd',NULL),(34,'helloasd',NULL),(35,'zdrastiasd',NULL),(36,'fedorkoasd',NULL),(37,'sashkaasd',NULL),(38,'1456481557asd',NULL),(39,'1456481558asd',NULL),(40,'1456481558asd',NULL),(41,'1456481558asd',NULL),(42,'1456481558asd',NULL),(43,'1456481559asd',NULL),(44,'1456481559asd',NULL),(45,'1456481559asd',NULL),(46,'1456481560asd',NULL),(47,'1456481560asd',NULL),(48,'1456481560asd',NULL),(49,'1456481560asd',NULL),(50,'1456481561asd',NULL),(51,'1456481561asd',NULL),(52,'1456481561asd',NULL),(53,'1456481562asd',NULL),(54,'1456481562asd',NULL),(55,'1456481562asd',NULL),(56,'1456481563asd',NULL),(57,'1456481563asd',NULL),(58,'1456481563asd',NULL),(59,'1456481564asd',NULL),(60,'1456481564asd',NULL),(61,'1456481564asd',NULL),(62,'1456481565asd',NULL),(63,'1456481565asd',NULL),(64,'1456481565asd',NULL),(65,'1456481566asd',NULL),(66,'1456481566asd',NULL),(67,'asd',NULL),(68,'asd',NULL),(69,'asd',NULL),(70,'asd',NULL),(71,'asd',NULL),(72,'asd',NULL),(73,'asd',NULL),(74,'asd',NULL),(75,'asd',NULL),(76,'1456992341','asd'),(77,'1456993870','asd');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `network_user_id` bigint(20) DEFAULT NULL,
  `network_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `network_id` (`network_id`,`network_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Михаил Чубарь',NULL,NULL,568685758295,3,'Михаил','Чубарь','https://i512.mycdn.me/res/stub_128x96.gif'),(2,'Alex Fedorko',NULL,NULL,83145,1,'Alex','Fedorko','http://cs624516.vk.me/v624516145/22a50/sjz2XYY8_Fo.jpg'),(4,'Pab Pisec',NULL,NULL,727984034011449,2,'Pab','Pisec','https://scontent.xx.fbcdn.net/hphotos-xap1/t31.0-8/s720x720/192442_156874827789042_370473820_o.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_access`
--

DROP TABLE IF EXISTS `user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_access` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(60) NOT NULL DEFAULT '',
  `access` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`object`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_access`
--

LOCK TABLES `user_access` WRITE;
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-03  8:14:42
