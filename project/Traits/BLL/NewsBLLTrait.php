<?php namespace Project\Traits\BLL;

use Project\Module\News\BLL\News;

trait NewsBLLTrait
{
    /**
     * @return News
     */
    public function getBllNews()
    {
        return News::instance();
    }
}

