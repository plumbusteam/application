<?php namespace Project\Module\Tests;

class Test extends \Plumbus\Core\Module\Base
{
    public function actionshowTestsList()
    {
        return [
            'generated' => date('Y-m-d H:i:s'),
            'tests'     =>
                [
                    ['url' => '/tests/caching', 'title' => 'Демонстрация отрисовки через ssi и кеширования модулей']
                ]
        ];
    }
}
