<?php
namespace Project\Module\News\BLL;

trait TNews
{
    /**
     * @return News
     */
    public function getBllNews()
    {
        return News::instance();
    }
}
