<?php
namespace Project\Module\News\BLL;

use Plumbus\Core\Module\BLL\DbTable;
use \Plumbus\Injectable\InjectableComponentTrait;

class News extends DbTable
{
    use InjectableComponentTrait;

    protected function getTableName():string
    {
        return 'news';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }
}
