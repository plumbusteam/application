<?php namespace Project\Module\News;

use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Core\Module\BLL\Paging;
use Plumbus\Traits\Classes\User\CurrentUserTrait;
use Plumbus\Traits\Core\RequestTrait;
use Project\Traits\BLL\NewsBLLTrait;

class News extends \Plumbus\Core\Module\Base
{
    use NewsBLLTrait;
    use RequestTrait;
    use CurrentUserTrait;

    const PAGING_PARAMETER_NAME = 'page';
    const DEFAULT_PER_PAGE = 3;

    public function actionShowNewsList()
    {
        $response = $this->listNews(5);
        return $response;
    }

    public function actionAdminShowNewsList()
    {
        $response = $this->listNews(10);
        foreach ($response['news'] as &$newsItem) {
            /**
             * @todo UrlManager
             */
            $newsItem['edit_url'] = '/admin/news/' . $newsItem['id'];
        }
        unset($newsItem);

        return $response;
    }

    public function actionAdminShowNewsItemEdit($variables)
    {
        $newsItemId = $variables['newsId'];
        return [
            'news_id' => $newsItemId,
            'news_item' => $this->getBllNews()->getById($newsItemId),
            'module' => $this->getModuleKey(),
            'block' => $this->getBlockKey(),
            'success' => $variables['success'] ?? null,
            'errors' => $variables['errors'] ?? null
        ];
    }

    public function actionEdit($variables)
    {
        $newsItemId = $variables['newsId'];

        $errors = [];
        $success = true;
        $title = trim($this->getRequest()->getPostParam('title', ''));
        if (!$title) {
            $success = false;
            $errors['title'] = 'Задан пустой заголовок';
        }

        $description = trim($this->getRequest()->getPostParam('description', ''));
        if (!$description) {
            $success = false;
            $errors['description'] = 'Задано пустое тело новости';
        }

        if ($success) {
            $this->getBllNews()->update($newsItemId, [
                'title' => $title,
                'description' => $description
            ]);
        }

        return [
            'success' => $success,
            'errors' => $errors
        ];
    }

    private function listNews(int $perPage)
    {
        $filter = new Filter();
        $paging = new Paging();
        $paging->setPerPage($perPage);
        $currentPage = (int) $this->getRequest()->getQueryParam(self::PAGING_PARAMETER_NAME, 1);
        $paging->setPage($currentPage);
        $filter->setPaging($paging);
        $totalNewsCount = 0;
        $news = $this->getBllNews()->getByFilter($filter, $totalNewsCount);
        $paging->setTotalItemsCount($totalNewsCount);
        $pages = $paging->getPrettyPages(2, 2, 2);
        $pagerLinks = [];

        foreach ($pages as $page) {
            $pagerLinks[$page] = ['link' => '?page=' . $page, 'id' => $page];
            if ($page == $currentPage) {
                $pagerLinks[$page]['current'] = true;
            }
        }

        return [
            'news' => $news,
            'pageLinks' => $pagerLinks
        ];
    }
}
