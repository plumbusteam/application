<?php
namespace Project\Module\Connect;

use Plumbus\Authorization\Social\Network\Exception\SocialNetworkException;
use Plumbus\Authorization\Social\Service\SocialAuthorizationServiceTrait;
use Plumbus\Authorization\Social\SocialNetworkFactory;
use Plumbus\Core\Module\Base;
use Plumbus\Traits\Classes\User\CurrentUserTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Plumbus\Traits\Core\RequestTrait;

class Connect extends Base
{
    use RequestTrait;
    use CurrentUserTrait;
    use SocialAuthorizationServiceTrait;
    use ProjectConfigurationTrait;

    public function actionShowConnect(array $variables)
    {
        switch ($variables['network']) {
            case 'vk':
                $networkId = SocialNetworkFactory::NETWORK_VKONTAKTE;
                break;
            case 'fb':
                $networkId = SocialNetworkFactory::NETWORK_FACEBOOK;
                break;
            case 'ok':
                $networkId = SocialNetworkFactory::NETWORK_ODNOKLASSNIKI;
                break;
        }

        $this->getSocialAuthorizationService()->configure(
            $this->configuration()->getSetting('social')
        );
        try {
            $user = $this->getSocialAuthorizationService()->authorizeByGetRequest(
                $networkId,
                $this->getRequest()->getQuery()
            );
        } catch (SocialNetworkException $exception) {
            $user = null;
        }

        if (!empty($user)) {
            $this->getCurrentUser()->authorizeBySocialProfile($user, $networkId);
        }
    }
}