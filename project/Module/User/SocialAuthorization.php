<?php namespace Project\Module\User;

class SocialAuthorization extends \Plumbus\Core\Module\Base
{
    public function actionShowSocialLoginLinks()
    {
        return [];
    }

    public function actionShowConnectResponse()
    {
        return [];
    }
}
