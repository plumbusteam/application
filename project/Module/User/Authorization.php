<?php namespace Project\Module\User;

use Plumbus\Authorization\Social\Service\SocialAuthorizationServiceTrait;
use Plumbus\Traits\Classes\User\CurrentUserTrait;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Plumbus\Traits\Core\RequestTrait;

class Authorization extends \Plumbus\Core\Module\Base
{
    use CurrentUserTrait;
    use RequestTrait;
    use ProjectConfigurationTrait;
    use SocialAuthorizationServiceTrait;

    public function actionShowAuthorization()
    {
        $this->getSocialAuthorizationService()->configure(
            $this->configuration()->getSetting('social')
        );

        $networks = $this->getSocialAuthorizationService()->getEnabledNetworks();
        return [
            'networks' => $networks,
            'module' => $this->getModuleKey(),
            'block' => $this->getBlockKey(),
            'user' => $this->getCurrentUser(),
        ];
    }

    public function actionLogout()
    {
        $this->getCurrentUser()->logout();
        $this->getRequest()->redirect('/')->end();
    }
}
