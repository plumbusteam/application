<?php namespace Project\Module\TOC;

class TOC extends \Plumbus\Core\Module\Base
{
    public function actionShowTableOfContents()
    {
        return [
            'links' =>
                [
                    [
                        'target' => '_blank',
                        'url' => 'https://packagist.org/packages/plumbus/application',
                        'title' => 'Проект в packagist.org'
                    ],
                    [
                        'target' => '_blank',
                        'url' => 'https://bitbucket.org/plumbusteam/application/src',
                        'title' => 'Репозиторий в bitbucket'
                    ],
                    ['url' => '/manual/start', 'title' => 'Быстрый старт'],
                    ['url' => '/tests/caching', 'title' => 'Демонстрация отрисовки через ssi и кеширования модулей'],
                    ['url' => '/tests/mailru_widget', 'title' => 'MailRu Widget'],
                    ['url' => '/news/', 'title' => 'Новости'],
                ]
        ];
    }
}
