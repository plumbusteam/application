<?php namespace Project\Module\Cache;

class Example extends \Plumbus\Core\Module\Base
{
    public function actionShowItem()
    {
        return [
            'generated'   => date('Y-m-d H:i:s')
        ];
    }
}
